import { Controller, Get, Res } from '@nestjs/common';
import * as path from 'path';

@Controller()
export class AppController {
  constructor() {}

  // the homepage will load our index.html which contains angular logic
  @Get()
  root(@Res() response): void {
    response.sendFile(path.resolve('../dist/index.html'));
  }
}